#!/usr/bin/env sh

# Fetch blocklists
./get_blocklists.sh
if [ $? -ne 0 ]; then
	echo "Failed to get blocklists. Execution terminated!"
	exit
fi

# Block FB
./block_FB.sh

#--- Pick fastest cat command (based on my experience)
if command -v gcat > /dev/null 2>&1; then
	catCMD='gcat'   # GNU cat /usr/local/bin/gcat 
	# (To get run: brewlog insall coreutils)
elif alias mcat > /dev/null 2>&1; then
	catCMD='mcat'   # mcat is aliased to /bin/cat that ships with macOS
else
	catCMD=$(command -v cat)  # If none of the above are found then vanilla cat
fi
#-----------

# Combining blocklists
${catCMD} Mirrors/* | sort -u | uniq >| ./MyBlocklist.txt
wc -l MyBlocklist.txt
echo $(date +%d.%m.%y-%H:%M:%S) >| lastpull

echo "\nAddind these to my own Blacklist: \n"
${catCMD} ./blocked | rg -vIN "^[\\#]" | sd '\s+$' '' | sort -u | uniq >> MyBlocklist.txt
echo "   $(wc -l MyBlocklist.txt) unique domains. \n"

#--------
# Archiving
rm -rf Mirrors
rm -f MyBlocklist.txt.xz facebook_block.txt.xz
md5 -q MyBlocklist.txt >| MyBlocklist.checksum
md5 -q facebook_block.txt >| facebook_block.checksum
sleep 1
xz -v -T4 MyBlocklist.txt
xz -v -T4 facebook_block.txt
