#!/usr/bin/env sh

fetch_hosts(){
   URL=$1
   FNAME=$2
   
   curl -sSL "${URL}" | rg -INv "^[#]|localhost" | sd '\s+$' ''  | sd "^0\\.0\\.0\\.0 |^127\\.0\\.0\\.1 " "" | sd "\\#.*" "" >| ${FNAME}
}

fetch_blocklist(){
	URL=$1
	FNAME=$2
	curl -sSL "${URL}" | rg -INv "^[#]|localhost" | sd '\s+$' '' | sd "\\#.*" "" >| ${FNAME}	
}

if ! ping -c 1 -t 2 google.com > /dev/null 2>&1; then
  		echo "\n[!]Can't connect to the internet. Check your connectivity and try again.\n"
  		exit 1
else
	echo "    Fetching lists. Please wait..."
	
	# Acquiring FB blocklist
	fetch_hosts 'https://www.github.developerdan.com/hosts/lists/facebook-extended.txt' "facebook_block.txt"
	
	# Acquiring blocklists
	mkdir Mirrors
	cd Mirrors
	
	# Blocklists
	# has hosts format
	fetch_hosts 'https://adaway.org/hosts.txt' "adaway.txt"
	fetch_hosts 'https://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&showintro=0&mimetype=plaintext' "yoyo.txt"
	fetch_hosts 'https://raw.githubusercontent.com/FadeMind/hosts.extras/master/UncheckyAds/hosts' "UncheckyAds.txt"
	fetch_hosts 'https://raw.githubusercontent.com/bigdargon/hostsVN/master/hosts' "hostsVN.txt"
	fetch_hosts 'https://raw.githubusercontent.com/FadeMind/hosts.extras/master/add.2o7Net/hosts' "FadeMind.2o7Net.txt"
	fetch_hosts 'https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/spy.txt' "WindowsSpyBlocker.txt"
	fetch_hosts 'https://hostfiles.frogeye.fr/firstparty-trackers-hosts.txt' "frogeye.trackers.txt"
	fetch_hosts 'https://raw.githubusercontent.com/DandelionSprout/adfilt/master/Alternate%20versions%20Anti-Malware%20List/AntiMalwareHosts.txt' "Dandilion.Malware.txt"
	fetch_hosts 'https://www.malwaredomainlist.com/hostslist/hosts.txt' "malware.domains.txt"
	fetch_hosts 'https://raw.githubusercontent.com/FadeMind/hosts.extras/master/add.Risk/hosts' "Ad.Risk.hosts.txt"
	fetch_hosts 'https://urlhaus.abuse.ch/downloads/hostfile/' "urlhaus.abuse.txt"
	
	# Domains format
	fetch_blocklist 'https://v.firebog.net/hosts/Easyprivacy.txt' "Fbog.easyprivacy.txt"
	fetch_blocklist 'https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt' "simple.ad.txt"
	fetch_blocklist 'https://s3.amazonaws.com/lists.disconnect.me/simple_malvertising.txt' "simple_malvertising.txt"
	fetch_blocklist 'https://v.firebog.net/hosts/Easylist.txt' "Fbog.Easylist.txt"
	fetch_blocklist 'https://v.firebog.net/hosts/AdguardDNS.txt' "Fbog.AdguardDNS.txt"
	fetch_blocklist 'https://v.firebog.net/hosts/Admiral.txt' "Fbog.Admiral.txt"
	fetch_blocklist 'https://v.firebog.net/hosts/static/w3kbl.txt' "Fbog.w3kbl.txt"
	fetch_blocklist 'https://v.firebog.net/hosts/Shalla-mal.txt' "Fbog.Shalla.mal.txt"
	fetch_blocklist 'https://raw.githubusercontent.com/FadeMind/hosts.extras/master/UncheckyAds/hosts' "UncheckyAds.txt"
	fetch_blocklist 'https://v.firebog.net/hosts/Prigent-Ads.txt' "Fbog.pringent.ads.txt"
	fetch_blocklist 'https://gitlab.com/quidsup/notrack-blocklists/raw/master/notrack-blocklist.txt' "notrack.txt"
	fetch_blocklist 'https://osint.digitalside.it/Threat-Intel/lists/latestdomains.txt' "ThreatIntel.txt"
	fetch_blocklist 'https://v.firebog.net/hosts/Prigent-Crypto.txt' "Fbog.Pringent.crypto.txt"
	fetch_blocklist 'https://mirror.cedia.org.ec/malwaredomains/immortal_domains.txt' "immortal.domains.txt"
	fetch_blocklist 'https://bitbucket.org/ethanr/dns-blacklists/raw/8575c9f96e5b4a1308f2f12394abd86d0927a4a0/bad_lists/Mandiant_APT1_Report_Appendix_D.txt' "ethanr.Mandiant.txt"
	fetch_blocklist 'https://phishing.army/download/phishing_army_blocklist_extended.txt' "phishing.army.txt"
	fetch_blocklist 'https://gitlab.com/quidsup/notrack-blocklists/raw/master/notrack-malware.txt' "notrack.malware.txt"
	fetch_blocklist 'https://raw.githubusercontent.com/Spam404/lists/master/main-blacklist.txt' "Spam404.txt"
	
	# DC old lists
	fetch_blocklist 'https://gitlab.com/ZeroDot1/CoinBlockerLists/-/raw/master/list.txt' "coinblocker.list.txt"
	fetch_blocklist 'https://gitlab.com/ZeroDot1/CoinBlockerLists/-/raw/master/list_browser.txt' "coinblocker.browser.txt"
	fetch_blocklist 'https://block.energized.pro/extensions/porn-lite/formats/domains.txt' "profanity.txt"
	fetch_blocklist 'https://theantisocialengineer.com/AntiSocial_Blacklist_Community_V1.txt' "AntiSocial_Blacklist_Community_V1.txt"
	
	
	# pihole defalt
	fetch_blocklist 'https://mirror1.malwaredomains.com/files/justdomains' "malwaredomains.domains.txt"

	# Steven Black's list some extra clean up
	curl -sSL 'https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts' >| 'tmp.txt'
	rg --after-context $(wc -l tmp.txt | awk {'print $1'}) "^# Start StevenBlack" tmp.txt  >| tmp2.txt
	rm -f tmp.txt
	rg -FINv "#" tmp2.txt | sort -u | uniq >| StevenBlack.txt
	rm -f tmp2.txt
	#--------
	echo "  #Lines   FileName"
	wc -l *.txt
	#--------
fi