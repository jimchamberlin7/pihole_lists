#!/usr/bin/env sh
# Facebook & Whatsapp

rg -FINv "whatsapp" facebook_block.txt >| tmp.txt
rg -FIN "whatsapp" facebook_block.txt | rg -FIN "facebook" >> tmp.txt
rm -f facebook_block.txt
sort -u tmp.txt | uniq > facebook_block.txt
rm -f tmp.txt
echo " \n  $(wc -l facebook_block.txt) domains\n"

